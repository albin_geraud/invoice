import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {Invoice} from '../models/invoice';
import {UpdateserviceService} from '../details/services/updateservice.service';
import {DomSanitizer} from '@angular/platform-browser';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  @Input() invoicelist: Invoice[];
  @Output() invoiceselected = new EventEmitter<any>();
  beftaxsum: number = 0;
  taxcost: number = 0;
  totalnet: number = 0;
  downloadJsonHref;
  currentInvoice: Invoice;
  json = require('./file.json');


  constructor(private  updateservice: UpdateserviceService, private sanitizer: DomSanitizer) {
    this.updateservice.detailsUpdated
      .subscribe(() => {
        this.totalnet = 0;
        console.log('update available');
        for (const invoice of this.invoicelist) {
          this.totalnet = this.totalnet + invoice.netsum;
        }
        this.taxcost = 19 * this.totalnet / 100;
        this.beftaxsum = this.totalnet + this.taxcost;
      });


  }

  ngOnInit() {
    console.log(this.invoicelist);
  }


  addinvoice() {
    this.invoicelist.push(new Invoice());
  }

  onselectinvoice(event) {
    console.log(event);
    this.currentInvoice = event;
    this.invoiceselected.emit(event);

  }

  generateDownloadJsonUri() {
    const theJSON = JSON.stringify(this.currentInvoice);
    const uri = this.sanitizer.bypassSecurityTrustUrl('data:text/json;charset=UTF-8,' + encodeURIComponent(theJSON));
    console.log(this.currentInvoice);
    this.downloadJsonHref = uri;
  }

  import() {
    console.log(this.json.customer_name);
    const newinvoice = new Invoice();
    newinvoice.customer_name = this.json.customer_name;
    newinvoice.customer_id = this.json.customer_id;
    newinvoice.customer_contact_person = this.json.customer_contact_person;
    newinvoice.customer_adress = this.json.customer_adress;
    newinvoice.customer_zip = this.json.customer_zip;
    newinvoice.customer_city = this.json.customer_city;
    newinvoice.iban = this.json.iban;
    newinvoice.bic = this.json.bic;
    newinvoice.account_owner = this.json.account_owner;
    newinvoice.mandate_reference = this.json.mandate_reference;
    newinvoice.mandate_city = this.json.mandate_city;
    newinvoice.mandate_date = this.json.mandate_date;
    newinvoice.mandate_signee = this.json.mandate_signee;
    newinvoice.invoice_number = this.json.invoice_number;
    newinvoice.invoice_period = this.json.invoice_period;
    newinvoice.invoice_date = this.json.invoice_date;
    newinvoice.line_items = this.json.line_items;
    this.invoicelist.push(newinvoice);

  }


}
