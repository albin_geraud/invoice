


export class Invoice {


  customer_id: number = 0;
  customer_name: String;
  customer_contact_person: String;
  customer_adress: String;
  customer_zip: String;
  customer_city: String;
  iban: String;
  bic: String;
  account_owner: String;
  mandate_reference: String;
  mandate_city: String;
  mandate_date: String;
  mandate_signee: String;
  invoice_number: String;
  invoice_period: String;
  invoice_date: String;
  invoice_due_date: String;
  line_items: Array<{ name: String, description: String, quantity: number, price_cents: number, position: number, summe: number}>;
  netsum: number;


  constructor() {

    this.customer_id = this.customer_id + 1;
    this.customer_name = 'new';
    this.customer_contact_person = '';
    this.customer_adress = '';
    this.customer_zip = '';
    this.customer_city = '';
    this.iban = '';
    this.bic = '';
    this.account_owner = '';
    this.mandate_reference = '';
    this.mandate_city = '';
    this.mandate_date = '';
    this.mandate_signee = '';
    this.invoice_number = '';
    this.invoice_period = '';
    this.invoice_date = '';
    this.invoice_due_date = '';
    this.line_items = [{name: '', description: '', quantity: 0, price_cents: 0, position: 1, summe: 0}];
    this.netsum = 0;


  }
}
