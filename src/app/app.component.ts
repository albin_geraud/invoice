import {Component} from '@angular/core';
import {Invoice} from './models/invoice';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  netsum: Invoice;


  invoicelist: Invoice[] = [];
  currentitem: Invoice;

  constructor() {

    this.invoicelist.push(new Invoice());
    console.log(this.invoicelist);
  }

  passinvoice(event) {
    this.currentitem = event;
  }


}
