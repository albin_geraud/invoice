import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';




import { AppComponent } from './app.component';
import {MatCardModule, MatIconModule, MatListModule, MatSelectModule, MatToolbarModule} from '@angular/material';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DetailsComponent } from './details/details.component';
import { ItemlistComponent } from './details/itemlist/itemlist.component';
import {FormsModule} from '@angular/forms';
import {UpdateserviceService} from './details/services/updateservice.service';


@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    DetailsComponent,
    ItemlistComponent
  ],
  imports: [
    BrowserModule,
    FormsModule

  ],
  providers: [UpdateserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
