import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Invoice} from '../../models/invoice';
import {UpdateserviceService} from '../services/updateservice.service';
import {DomSanitizer} from '@angular/platform-browser';


@Component({
  selector: 'app-itemlist',
  templateUrl: './itemlist.component.html',
  styleUrls: ['./itemlist.component.css']
})
export class ItemlistComponent implements OnInit {

  @Input() invoicelist: Invoice[];
  @Input() currentinvoice: Invoice;
  @Output() sendsum = new EventEmitter<any>();

  pos: number = 1;

  beftaxsum: number = 0;
  taxcost: number = 0;
  json: String;

  constructor(private updateservice: UpdateserviceService) {

  }

  ngOnInit() {
  }


  additems() {
    this.pos = this.pos + 1;
    this.currentinvoice.line_items.push({name: '', description: '', quantity: 0, price_cents: 0, position: this.pos, summe: 0});


  }

  deleteitems(item) {

    const index: number = this.currentinvoice.line_items.indexOf(item);
    if (index !== -1) {
      this.currentinvoice.line_items.splice(index, 1);
    }
    this.calculate();

    this.updateservice.detailsUpdated.emit();
  }


  netcalculate(item) {
    item.summe = item.quantity * item.price_cents;
    this.calculate();
    this.updateservice.detailsUpdated.emit();


  }

  calculate() {
    this.currentinvoice.netsum = 0;

    for (const i of this.currentinvoice.line_items) {

      this.currentinvoice.netsum = this.currentinvoice.netsum + i.summe;
    }
    this.taxcost = 19 * this.currentinvoice.netsum / 100;
    this.beftaxsum = this.currentinvoice.netsum + this.taxcost;

  }


}




