import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Invoice} from '../models/invoice';


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  @Input() invoicelist: Invoice[];
  @Input() currentinvoice: Invoice;




  constructor() {

  }

  ngOnInit() {
    for (const item of this.invoicelist) {
      if (item.customer_id === 1) {
        this.currentinvoice = item;
      }
    }
  }



}
